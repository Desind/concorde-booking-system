package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        BookingSystem concordeBookingSystem = new ConcordeBookingSystem();

        //Premium ticket id range 1-28
        //Standard ticket id range 29-128

        //Ordering new ticket
        System.out.println("Ordering new ticket");
        concordeBookingSystem.bookTicket(22,"Grzegorz Brzęczyszczykiewicz");
        ArrayList<Integer> bookedSeats = concordeBookingSystem.getBookedSeats();
        ArrayList<Integer> unbookedSeats = concordeBookingSystem.getUnbookedSeats();
        System.out.println("Booked seats: " + bookedSeats);
        System.out.println("Unbooked seats: " + unbookedSeats);
        System.out.println("Summary cashbox value: " + concordeBookingSystem.getCurrentCashboxStatus());

        //Relocation of ticket
        System.out.println("Relocation of ticket");
        concordeBookingSystem.changeSeat(22,3);
        bookedSeats = concordeBookingSystem.getBookedSeats();
        unbookedSeats = concordeBookingSystem.getUnbookedSeats();
        System.out.println("Booked seats: " + bookedSeats);
        System.out.println("Unbooked seats: " + unbookedSeats);

        //Cancelling ticket
        System.out.println("cancelling ticket");
        concordeBookingSystem.cancelTicket(3);
        bookedSeats = concordeBookingSystem.getBookedSeats();
        unbookedSeats = concordeBookingSystem.getUnbookedSeats();
        System.out.println("Booked seats: " + bookedSeats);
        System.out.println("Unbooked seats: " + unbookedSeats);

    }
}
