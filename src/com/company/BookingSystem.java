package com.company;

import java.util.ArrayList;
class Seat{
    private int id;

    public void setOwner(String owner) {
        this.owner = owner;
    }

    private String owner;
    private BookingSystem.SeatType type;

    public Seat(int id, String owner, BookingSystem.SeatType type) {
        this.id = id;
        this.owner = owner;
        this.type = type;
    }
    boolean isBooked(){
        if(this.owner == null) return false;
        return true;
    }

    public int getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public BookingSystem.SeatType getType() {
        return type;
    }
}
public interface BookingSystem {

    enum SeatType{
        STANDARD,
        VIP
    }

    int getVIPTicketPrice();
    int getStandardTicketPrice();

    boolean bookTicket(int id, String owner);
    boolean cancelTicket(int id);

    float getCurrentCashboxStatus();
    ArrayList<Integer> getUnbookedSeats();
    ArrayList<Integer> getBookedSeats();
    boolean changeSeat(int previousSeat, int newSeat);
}
