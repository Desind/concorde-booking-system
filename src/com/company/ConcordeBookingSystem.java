package com.company;

import java.util.ArrayList;

public class ConcordeBookingSystem implements BookingSystem{
    ArrayList<Seat> seats = new ArrayList<>();
    public ConcordeBookingSystem(){
        for(int i=1; i<29; i++){
            seats.add(new Seat(i, null, BookingSystem.SeatType.VIP));
        }
        for(int i=29; i< 129; i++){
            seats.add(new Seat(i,null, SeatType.STANDARD));
        }
    }
    @Override
    public int getVIPTicketPrice() {
        return 1200;
    }

    @Override
    public int getStandardTicketPrice() {
        return 800;
    }

    @Override
    public boolean bookTicket(int id, String owner) {
        if(!seats.get(id - 1).isBooked()){
            seats.get(id - 1).setOwner(owner);
            System.out.println(seats.get(id - 1));
            return true;
        }
        return false;
    }

    @Override
    public boolean cancelTicket(int id) {
        if(seats.get(id - 1).isBooked()){
            seats.get(id - 1).setOwner(null);
            return true;
        }
        return false;
    }

    @Override
    public float getCurrentCashboxStatus() {
        float cashboxValue = 0f;
        for(Seat x: this.seats){
            if(x.isBooked()){
                if(x.getType()==SeatType.STANDARD){
                    cashboxValue += getStandardTicketPrice();
                }else{
                    cashboxValue += getVIPTicketPrice();
                }
            }
        }
        return cashboxValue;
    }

    @Override
    public ArrayList<Integer> getUnbookedSeats() {
        ArrayList<Integer> availableSeats = new ArrayList<>();
        for (Seat x: this.seats) {
            if(!x.isBooked()){
                availableSeats.add(x.getId());
            }
        }
        return availableSeats;
    }

    @Override
    public ArrayList<Integer> getBookedSeats() {
        ArrayList<Integer> bookedSeats = new ArrayList<>();
        for (Seat x: this.seats) {
            if(x.isBooked()){
                bookedSeats.add(x.getId());
            }
        }
        return bookedSeats;
    }

    @Override
    public boolean changeSeat(int previousSeat, int newSeat) {
        if((!this.seats.get(newSeat-1).isBooked()) && this.seats.get(newSeat-1).getType() == this.seats.get(previousSeat+1).getType()){
            this.seats.get(newSeat-1).setOwner(this.seats.get(previousSeat-1).getOwner());
            cancelTicket(previousSeat);
            return true;
        }
        return false;
    }
}
